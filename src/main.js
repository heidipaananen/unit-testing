const express = require('express')
const app = express()
const port = 3000
const mylib = require('./mylib')

//endpoint localhost:3000/
app.get('/', (req, res) => {
  res.send('Hello World!')
})

//endpoint localhost:3000/add?a&b
app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    console.log({ a , b })
    const total = mylib.sum(a,b) // konfliktia ei tule eli a ja b eivät mene sekaisin main.js ja mylib.js tiedostoissa
    res.send('add works ' + total.toString())
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
